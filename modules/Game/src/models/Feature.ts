namespace MyGame.Models
{
    /**
     * Contains feature data specific to white label slot.
     */
    export interface Feature
    {
        /** Data about the in-reel feature, or null if no such feature is present. */
        inReelFeature: Feature.InReelFeature | null;
    }

    export namespace Feature
    {
        export namespace InReelFeature
        {
            /** Type of in-reel feature. */
            export enum Type
            {
                StackedKings
            }

            export interface Base<TType extends Type>
            {
                type: TType;
            }

            export interface StackedKings extends Base<Type.StackedKings>
            {
                reels: number[];
            }
        }

        export type InReelFeature = InReelFeature.StackedKings;

        export function clear(feature: Feature)
        {
            feature.inReelFeature = null;
        }
    }
}