namespace MyGame
{
    export interface Models extends RS.Slots.Models
    {
        feature: Models.Feature;
    }

    export namespace Models
    {
        export function clear(models: Models)
        {
            RS.Slots.Models.clear(models);
            models.feature = models.feature || {} as Models.Feature;
            Models.Feature.clear(models.feature);
        }
    }
}