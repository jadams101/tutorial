/// <reference path="sections.ts" />

namespace MyGame.Settings.Paytable 
{
    export const Paytable: SG.CommonUI.Views.Paytable.Settings =
    {
        ...SG.CommonUI.Views.Paytable.defaultSettings,
        sections: MyGame.Settings.Paytable.Sections
    }

    export class MyCustomPage extends RS.Slots.Paytable.Pages.Base<MyCustomPage.Settings, RS.Slots.Paytable.Pages.Base.RuntimeData>
    {
        private _settings: MyCustomPage.Settings;
        constructor(settings: MyCustomPage.Settings, runtime: RS.Slots.Paytable.Pages.Base.RuntimeData)
        {
            super(settings, runtime);
            this._settings = settings;
        }
    }
    export const myCustomPage = RS.Flow.declareElement(MyCustomPage, true);

    export namespace MyCustomPage
    {
        export interface Settings extends RS.Slots.Paytable.Pages.Base.Settings 
        {
            Test?: number
            // portraitLogo:
            // {
            //     kind: RS.Flow.Image.Kind.Bitmap,
            //     asset: MyGame.Assets.Logo,
            //     scaleMode: RS.Math.ScaleMode.Contain
            // },
            // landscapeLogo:
            // {
            //     kind: RS.Flow.Image.Kind.Bitmap,
            //     asset: MyGame.Assets.Logo,
            //     scaleMode: RS.Math.ScaleMode.Contain
            // },
            // leftButton:
            // {
            //     ...SG.CommonUI.Views.Paytable.defaultSettings.leftButton,
            //     hitArea: buttonHitArea,
            //     clickSound: Assets.SFX.UI.Menu.Back,
            //     background:
            //     {
            //         kind: RS.Flow.Background.Kind.ImageFrame,
            //         asset: Assets.Paytable,
            //         frame: Assets.Paytable.paytable_next_up,
            //     },
            //     pressbackground:
            //     {
            //         kind: RS.Flow.Background.Kind.ImageFrame,
            //         asset: Assets.Paytable,
            //         frame: Assets.Paytable.paytable_next_press,
            //     },
            // }
        }
    }
}