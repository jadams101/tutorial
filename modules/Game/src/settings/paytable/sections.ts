namespace MyGame.Settings.Paytable 
{
    export const Sections: SG.CommonUI.Views.Paytable.SectionInfo[] =
        [
            {
                title: "",
                pages:
                    [
                        {
                            title: Translations.Paytable.FreeSpins.Title,
                            element: RS.Slots.Paytable.Pages.textPage.bind
                                ({
                                    ...RS.Slots.Paytable.Pages.TextPage.defaultSettings,
                                    elements:
                                        [
                                            {
                                                kind: RS.Slots.Paytable.Pages.TextPage.ElementKind.DefaultText,
                                                text: Translations.Paytable.FreeSpins.Title
                                            }
                                        ]
                                })
                        },
                        // {
                        //     title: "Payout Page",
                        //     element: RS.Slots.Paytable.Pages.symbolPayouts.bind
                        //         ({
                        //             bindingSet: MyGame.Settings.S12345,
                        //             database: RS.Reels.Symbols.IDatabase.get(),
                        //             listSettings: RS.Flow.List.defaultSettings,
                        //             baseSymbolPayout: {
                        //                 //Total bet or bet per line
                        //                 /* useTotalStakeForPayouts: */
                        //                 //Following settings change how your payouts are styled and laid out
                        //                 /*
                        //                     indexLabel:
                        //                     valueLabel:
                        //                     listSettings:
                        //                     background:
                        //                 */
                        //                 //Add text to the payout page 
                        //                 //Set before or after the payouts on the page
                        //                 /* addAfterPayouts: */
                        //                 //Add an array of label settings
                        //                 /* text: */
                        //                 //Choose if we show the money value or the multiple of stake
                        //                 payoutsAsMultipliers: true,
                        //                 symbolSize: { w: 0.5, h: 0.5 },
                        //                 indexLabel: {
                        //                     ...RS.Flow.Label.defaultSettings,
                        //                     text: Translations.Paytable.FreeSpinsTitle
                        //                 },
                        //                 valueLabel: {
                        //                     ...RS.Flow.Label.defaultSettings,
                        //                     text: Translations.Paytable.FreeSpinsTitle
                        //                 },
                        //                 payoutSpacing: {
                        //                     left: 2,
                        //                     top: 2,
                        //                     right: 2,
                        //                     bottom: 2
                        //                 }
                        //             },
                        //             symbols: { ...RS.Reels.Symbols.randomSelectSymbol }
                        //         })
                        // },
                        {
                            title: "Winlines",
                            element: new RS.Slots.Paytable.Pages.WinlinePageFactory(RS.Slots.Paytable.Pages.Winlines,
                                {
                                    pageSettings:
                                    {
                                        ...RS.Slots.Paytable.Pages.Winlines.defaultSettings,
                                        listSettings: RS.Flow.List.defaultSettings,
                                        winlineSettings:
                                        {
                                            ...RS.Slots.Paytable.Elements.WinLine.defaultSettings,
                                            dock: RS.Flow.Dock.Top,
                                            gridSettings:
                                            {
                                                ...RS.Slots.Paytable.Elements.WinLine.defaultSettings.gridSettings,
                                                reelCount: 5,
                                                rowCount: 10,
                                                blockSize: 13,
                                                bgColor: new RS.Util.Color(0x5b5b5b),
                                                bgBitmap: SG.CommonUI.Assets.CommonUI.Paytable.Background.Landscape,
                                                bgBitmapOffset: RS.Math.Vector2D(0, 0),
                                                gridColor: new RS.Util.Color(0x700f0f),
                                                blockColor: new RS.Util.Color(0xeac02a),
                                                lineColor: new RS.Util.Color(0xea2a2a),
                                                lineOutlineColor: new RS.Util.Color(0x700f0f),
                                                lineWidth: 4,
                                                expand: RS.Flow.Expand.Disallowed,
                                                dock: RS.Flow.Dock.Top
                                            },
                                            label:
                                            {
                                                ...RS.Slots.Paytable.Elements.WinLine.defaultSettings.label,
                                                font: SG.CommonUI.Assets.Myriad.Black,
                                                dock: RS.Flow.Dock.Bottom,
                                                seleniumId: "Win Lines",
                                                layers:
                                                    [
                                                        {
                                                            layerType: RS.Rendering.TextOptions.LayerType.Border,
                                                            fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                                                            size: 3,
                                                            color: RS.Util.Colors.black
                                                        },
                                                        {
                                                            layerType: RS.Rendering.TextOptions.LayerType.Fill,
                                                            fillType: RS.Rendering.TextOptions.FillType.SolidColor,
                                                            color: RS.Util.Colors.white
                                                        }
                                                    ]
                                            }
                                        }
                                    },
                                    winLinesPerPage: 40

                                })
                        },
                        {
                            title: "Custom Page",
                            element: MyGame.Settings.Paytable.myCustomPage.bind({

                            })
                        }
                    ]
            }
        ]
}