/// <reference path="../views/MainSlot.ts" />
/// <reference path="../engine/DummyEngine.ts" />

namespace MyGame.Settings
{
    export const Game: MyGame.Game.Settings =
    {
        landscape:
        {
            dimensions: { w: 1365, h: 1024 },
            stageDimensions: { w: 2048, h: 1024 }
        },
        portrait:
        {
            dimensions: { w: 1365, h: 2048 },
            stageDimensions: { w: 2048, h: 2048 }
        },
        replayDialogDisplayTime: 1000,
        assetGroupPriorities:
        {
            [SG.CommonUI.Assets.Groups.fonts]: RS.Asset.LoadPriority.Immediate,
            [SG.CommonUI.Assets.Groups.commonUI]: RS.Asset.LoadPriority.Preload,
            [MyGame.Assets.Groups.mainSlot]: RS.Asset.LoadPriority.Preload,
            [MyGame.Assets.Groups.symbols]: RS.Asset.LoadPriority.Preload
        },
        primaryView: MyGame.Views.MainSlot,
        loadingView: RS.Views.Loading,
        paytableView: SG.CommonUI.Views.Paytable,
        stateMachine:
        {
            initialState: null,
            states:
                [
                    RS.Slots.GameState.LoadingState,
                    RS.Slots.GameState.ResumingState,
                    RS.Slots.GameState.IdleState,
                    RS.Slots.GameState.PreSpinState,
                    RS.Slots.GameState.SpinningState,
                    RS.Slots.GameState.WinRenderingState,
                    RS.Slots.GameState.BigWinState,
                    RS.Slots.GameState.PlayFinishedState
                ]
        },
        engine: MyGame.Engine.DummyEngine,
        engineSettings: MyGame.Engine.DummyEngine.defaultSettings,
        devTools:
        {
            ...RS.Slots.DevTools.defaultSettings
        }
    };
}
