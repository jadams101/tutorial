/// <reference path="../../generated/Assets.ts" />
/// <reference path="../Reels.ts" />

namespace MyGame.Settings.Views
{
    export const MainSlot: MyGame.Views.MainSlot.Settings =
    {
        landscape:
        {
            reelsPos: { x: 0.5, y: 0.5 },
            background:
            {
                dock: RS.Flow.Dock.Fill,
                kind: RS.Flow.Background.Kind.Image,
                asset: Assets.MainSlot.Background
            },
        },
        portrait:
        {
            reelsPos: { x: 0.5, y: 0.7 },
            background:
            {
                dock: RS.Flow.Dock.Fill,
                kind: RS.Flow.Background.Kind.Image,
                asset: Assets.MainSlot.Background.Portrait
            },
        },
        reels: ReelsComponentSettings,
        winlines:
        {
            colors: RS.Slots.Util.generateWinlineColors(5, 0.9),
            configModel: null
        },
        devTools:
        {
            ...RS.Slots.DevTools.defaultSettings,
            forceData: {
                //Map of strings that need to be stringified
            }
        }
    };

}