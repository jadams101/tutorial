
/// <reference path="settings/Game.ts" />
/// <reference path="settings/views/MainSlot.ts" />

namespace MyGame
{
    export let game: Game;

    export class Game extends RS.Slots.Game
    {
        //Protected Types
        protected _devToolsController: RS.Slots.DevTools.IController;
        protected _models: Models;
        //Getters
        public get devToolsController() { return this._devToolsController; }
        public get models() { return this._models; }

        public constructor(public readonly settings: Game.Settings)
        {
            super(settings);
            RS.Storage.init("TestGame", "{!GAME_VERSION!}");
        }

        @RS.Init() private static init()
        {
            game = new Game(Settings.Game);
            game.run();
        }

        //Protected Methods
        protected createControllers()
        {
            super.createControllers();

            this._devToolsController = RS.Slots.DevTools.IController.get();
            this._devToolsController.initialise({
                ...this.settings.devTools,
                engine: this._engine,
                canSpinArbiter: this.arbiters.canSpin,
                pauseArbiter: this.arbiters.pause,
                viewController: this._viewController
            });
        }

        //binds the view to MainSlot
        protected initialiseViews()
        {
            super.initialiseViews();
            this._viewController.bindSettings(Views.MainSlot, Settings.Views.MainSlot);
            this._viewController.bindSettings(SG.CommonUI.Views.Paytable, Settings.Paytable.Paytable);
        }

        protected initialiseModels()
        {
            this._models = {} as Models;
            Models.clear(this._models);
        }
    }

    export namespace Game
    {
        export interface Settings extends RS.Slots.Game.Settings
        {
            devTools: RS.Slots.DevTools.IController.Settings;
            Test?: number;
        }

        export interface Context extends RS.Slots.Game.Context
        {
            game: Game;
        }
    }
}