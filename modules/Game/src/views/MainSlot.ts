/// <reference path="../generated/Assets.ts" />
/// <reference path="../settings/Reels.ts" />

namespace MyGame.Views
{
    /**
     * The MainSlot view.
     */
    @RS.View.RequiresAssets([MyGame.Assets.Groups.mainSlot, MyGame.Assets.Groups.symbols]) // TODO: Add any required asset groups here
    // @RS.View.ScreenName("") // TODO: Add screen name here
    export class MainSlot extends RS.View.Base<MainSlot.Settings, Game.Context>
    {
        @RS.AutoDisposeOnSet protected _reels: RS.Reels.IGenericComponent;
        @RS.AutoDisposeOnSet protected _background: RS.Flow.Background;
        @RS.AutoDisposeOnSet protected _showButtonAreaHandle: RS.IDisposable;

        /**
         * Called when this view has been opened.
         */
        public onOpened()
        {
            super.onOpened();
            this._showButtonAreaHandle = this.context.game.arbiters.showButtonArea.declare(true);

            //Reels
            RS.Reels.Symbols.IDatabase.get().rebuild(this._controller);

            const models = this.context.game.models;
            const initialReelsState = models.config.defaultReels;
            this._reels = new RS.Reels.Component(this.settings.reels,
                {
                    skipArbiter: this.context.game.arbiters.canSkip,
                    initialConfiguration:
                    {
                        reelSet: models.config.reelSets[initialReelsState.currentReelSetIndex],
                        symbolsInView: initialReelsState.symbolsInView,
                        stopIndices: initialReelsState.stopIndices
                    }
                });
            this.context.primaryReels = this._reels;

            //WinLines
            this.context.winRenderer = new RS.Slots.WinRenderers.Composite([
                new RS.Slots.WinRenderers.SymbolAnimator(this._reels, this._reels),
                new RS.Slots.WinRenderers.Winline({
                    ...this.settings.winlines,
                    configModel: this.context.game.models.config
                }, this._reels, this._reels),
                new RS.Slots.WinRenderers.WinAmountUpdate({
                    winAmount: this.context.game.observables.win,
                    type: RS.Slots.WinRenderers.WinAmountUpdate.UpdateType.Immediate
                }),
                new RS.Slots.WinRenderers.StatusText({
                    configModel: this.context.game.models.config,
                    locale: this.context.game.locale,
                    currencyFormatter: this.context.game.currencyFormatter,
                    statusTextArbiter: this.context.game.arbiters.messageText
                }),
                new RS.Slots.WinRenderers.SymbolFade(this._reels)
            ]);

            //Dev Tools
            if (RS.IPlatform.get().shouldDisplayDevTools && !this.context.game.models.state.isReplay)
            {
                // Dev tools
                this.context.game.devToolsController.create(this.context.game.gameVersion);
                this.context.game.devToolsController.reels = this._reels;
                this.context.game.devToolsController.attach(this.visibleRegionPanel, {
                    dock: RS.Flow.Dock.Float,
                    floatPosition: { x: 0.0, y: 0.45 },
                    dockAlignment: { x: 0.0, y: 0.5 }
                });
            }
            this.addChild(this.visibleRegionPanel);
            this.moveToTop(this.visibleRegionPanel);
            this.handleOrientationChanged(RS.Orientation.currentOrientation);
        }

        /**
         * Called when this view has been closed.
         */
        public onClosed()
        {
            super.onClosed();
            this.context.primaryReels = null;
            this.context.primaryReels = null;
            this.context.winRenderer = null;
        }

        protected setupOrientation(settings: MainSlot.OrientationSettings): void
        {
            this._background = RS.Flow.background.create(settings.background, this);
            this._background.addChild(this._reels);
        }

        protected handleOrientationChanged(newOrientation: RS.DeviceOrientation): void 
        {
            super.handleOrientationChanged(newOrientation);
            if (newOrientation === RS.DeviceOrientation.Landscape)
            {
                this.setupOrientation(this.settings.landscape);
            } else
            {
                this.setupOrientation(this.settings.portrait);
            }
            this.addChild(this.visibleRegionPanel);
            this.moveToTop(this.visibleRegionPanel);
        }

    }

    export namespace MainSlot
    {
        export interface OrientationSettings
        {
            background: RS.Flow.Background.Settings;
            reelsPos: RS.Math.Vector2D;
        }

        export interface Settings extends RS.View.Base.Settings
        {
            devTools: RS.Slots.DevTools.IController.Settings;
            winlines: RS.Slots.WinRenderers.Winline.Settings;
            reels: RS.Reels.IComponent.Settings;
            landscape: OrientationSettings;
            portrait: OrientationSettings;
            Test?: number
        }
    }
}