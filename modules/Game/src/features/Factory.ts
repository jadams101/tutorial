
namespace MyGame.Features
{
    export class Factory implements RS.Slots.IFeatureFactory 
    {
        /* Creates one or more features given the specified game context */
        public create(context: Game.Context): RS.Slots.IFeature[] | null 
        {
            const result: RS.Slots.IFeature[] = [];
            const models = context.game.models;
            if (models.feature.inReelFeature && models.feature.inReelFeature.type === Models.Feature.InReelFeature.Type.StackedKings)
            {
                result.push(new StackedWilds(context));
            }
            return result
        }
    }
    RS.Slots.IFeatureFactory.register(Factory);
}