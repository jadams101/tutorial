/// <reference path="../settings/Reels.ts" />
namespace MyGame.Features
{
    export class StackedWilds implements RS.Slots.IFeature
    {
        /** Whether or not autoplay should ignore this feature and not stop. */
        public get isNotStoppingAutoplay() { return false; }

        public constructor(public readonly context: RS.Slots.Game.Context) { }

        /** Called before anything else when this feature is created from a restored game. Optionally returns state to move to. */
        public onResume(): string | null
        {
            return null;
        }

        /** Called when the resume dialog is closed after the main view has been opened. Optionally returns state to move to. */
        public onResumeDialogClosed(): string | null
        {
            return null;
        }

        /** Called to adjust the stop symbols of the reels before the reels have spun down. Useful for rigging certain results. */
        public adjustReels(reels: RS.Reels.IGenericComponent): void
        {
            return;
        }

        /** Called when the reels have reached maximum speed. Reels won't spin down until this is complete. Optionally returns state to move to. */
        public async onMidSpin(): Promise<string | null>
        {
            const models = MyGame.game.models;
            const featureData = models.feature.inReelFeature;
            if (featureData.type !== Models.Feature.InReelFeature.Type.StackedKings)
            {
                return null;
            }
            for (const reelIndex of featureData.reels) 
            {
                // RS.Log.debug("Hello!");
                await RS.ITicker.get().after(1000);
                this.context.primaryReels.stopReel(reelIndex, RS.Reels.IReel.StopType.Immediate);
            }
            return null;
        }

        /** Called when the reels have finished spinning down, but before big win or win rendering. Optionally returns state to move to. */
        public async onEndSpin(): Promise<string | null>
        {
            return null;
        }

        /** Called just before win rendering has started. */
        public async onPreWinRendering(): Promise<void>
        {
            return;
        }

        /** Called after win rendering has completed. Optionally returns state to move to. */
        public async onEndWinRendering(): Promise<string | null>
        {
            return null;
        }

        /** Called right at the end of the entire play, before moving to the next spin. Optionally returns state to move to. */
        public async onEndPlay(): Promise<string | null>
        {
            return null;
        }
    }
}