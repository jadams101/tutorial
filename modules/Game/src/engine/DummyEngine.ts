namespace MyGame.Engine
{
    export class DummyEngine extends RS.Slots.DummyEngine
    {
        protected _isStackedKings: boolean = false;

        public constructor(public readonly settings: DummyEngine.Settings, public readonly models: Models)
        {
            super(settings, models);
        }
        /** @inheritdoc */
        protected doLogic(stakePerLine: number, accumulatedWin: number): void 
        {
            // Decide if we're going to win stacked kings feature 
            this._isStackedKings = this._rng.random() < this.settings.chanceForStackedKings;

            //Do base Logic
            super.doLogic(stakePerLine, accumulatedWin);
        }

        /** @inheritdoc */
        protected spinReels(stakePerLine: number): RS.Slots.Models.SpinResult
        {
            // Determine stops
            const stops = this._force ? [...this._force[0].stopIndices] : this.getStops(true);
            const symbolsInView = this.sampleStops(stops);

            // Apply stacked kings feature
            if (this._isStackedKings)
            {
                const reelsToStack: number[] = [];
                for (let reelIndex = 0; reelIndex < 5; ++reelIndex)
                {
                    if (this._rng.random() < this.settings.stackedKingsChancePerReel)
                    {
                        reelsToStack.push(reelIndex);
                    }
                }
                if (reelsToStack.length === 0)
                {
                    reelsToStack.push(this._rng.randomIntRange(0, 5));
                }
                for (const stackIndex of reelsToStack)
                {
                    symbolsInView.setReel(stackIndex, [2, 2, 2]);
                }
                this.models.feature.inReelFeature = {
                    type: Models.Feature.InReelFeature.Type.StackedKings,
                    reels: reelsToStack
                };
            }
            else
            {
                this.models.feature.inReelFeature = null;
            }

            // Compute wins
            const paylineWins = this.computePaylineWins(stakePerLine, symbolsInView);

            // Assemble spin result
            const totalWin = paylineWins.map((win) => win.totalWin).reduce((a, b) => a + b, 0);
            const spinResult: RS.Slots.Models.SpinResult =
            {
                index: 0,
                reels:
                {
                    stopIndices: stops,
                    symbolsInView: symbolsInView,
                    currentReelSetIndex: 0
                },
                win:
                {
                    totalWin: totalWin,
                    accumulatedWin: totalWin,
                    paylineWins: paylineWins
                },
                balanceBefore: { primary: this._balance, named: {} },
                balanceAfter: { primary: this._balance, named: {} }
            };
            return spinResult;
        }

    }

    export namespace DummyEngine
    {
        export interface Settings extends RS.Slots.DummyEngine.Settings
        {
            chanceForStackedKings: number;
            stackedKingsChancePerReel: number;
        }
        export let defaultSettings: Settings =
        {
            ...RS.Slots.DummyEngine.defaultSettings,
            chanceForStackedKings: 0.25,
            stackedKingsChancePerReel: 0.5
        };
    }
}